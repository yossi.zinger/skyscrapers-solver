from unittest import TestCase
from SkyscrapersBoard import Skyscrapers, Edge


def create_three_by_three_board():
    """
     [[1 2 3]
     [3 1 2]
     [2 3 1]]
    """
    sk = Skyscrapers(3)
    sk.set_cell_value(row=0, column=0, value=1)
    sk.set_cell_value(row=0, column=1, value=2)
    sk.set_cell_value(row=0, column=2, value=3)
    sk.set_cell_value(row=1, column=0, value=3)
    sk.set_cell_value(row=1, column=1, value=1)
    sk.set_cell_value(row=1, column=2, value=2)
    sk.set_cell_value(row=2, column=0, value=2)
    sk.set_cell_value(row=2, column=1, value=3)
    sk.set_cell_value(row=2, column=2, value=1)
    return sk


class TestSkyscrapers(TestCase):
    def test_CanInitializeASquareBoard(self):
        for n in range(4, 7):
            sk = Skyscrapers(n)
            columns, rows = sk.get_board_size()
            self.assertEqual(n, columns)
            self.assertEqual(n, rows)

    def test_CanSetValueOfCellToNumber(self):
        sk = Skyscrapers(4)
        column = 0
        row = 0
        tower_height = 3
        sk.set_cell_value(column=column, row=row, value=tower_height)
        value = sk.get_cell_value(column=column, row=row)
        self.assertEqual(tower_height, value)

    def test_CellWithoutValueIsNegative(self):
        sk = Skyscrapers(4)
        column = 0
        row = 0
        value = sk.get_cell_value(column=column, row=row)
        self.assertLess(value, 0)

    def test_CanAddHints(self):
        sk = Skyscrapers(4)
        hint_value = 1
        column = 0
        sk.set_hint(row_or_column=column, edge=Edge.TOP, hint=hint_value)
        value = sk.get_hint(row_or_column=column, edge=Edge.TOP)
        self.assertEqual(hint_value, value)
        hint_value = 3
        row = 2
        sk.set_hint(row_or_column=row, edge=Edge.LEFT, hint=hint_value)
        value = sk.get_hint(row_or_column=row, edge=Edge.LEFT)
        self.assertEqual(hint_value, value)

    def test_CanSolveTrivialBoard(self):
        sk = Skyscrapers(1)
        sk.solve()
        value = sk.get_cell_value(0, 0)
        self.assertEqual(1, value)

    def test_CanSolveTwoByTwoWithOneHint(self):
        """
        [1,2
         2,1] <- 2
        """
        sk = Skyscrapers(2)
        sk.set_hint(row_or_column=1, edge=Edge.RIGHT, hint=2)
        sk.solve()
        value = sk.get_cell_value(row=0, column=0)
        self.assertEqual(1, value)
        value = sk.get_cell_value(row=0, column=1)
        self.assertEqual(2, value)
        value = sk.get_cell_value(row=1, column=0)
        self.assertEqual(2, value)
        value = sk.get_cell_value(row=1, column=1)
        self.assertEqual(1, value)

    def test_ComputeHintFromTowerRow(self):
        # 3 -> [1,2,4,3] <- 2
        row = [1, 2, 4, 3]
        hint = Skyscrapers.compute_hint(tower_row=row)
        self.assertEqual(3, hint)
        hint = Skyscrapers.compute_hint(tower_row=row[::-1])  # reversed
        self.assertEqual(2, hint)

    def test_GetListFromEdge(self):
        sk = create_three_by_three_board()
        row = sk.get_list_from_edge(row_or_column=0, edge=Edge.LEFT)
        self.assertEqual([1, 2, 3], row)
        row = sk.get_list_from_edge(row_or_column=1, edge=Edge.RIGHT)
        self.assertEqual([2, 1, 3], row)
        row = sk.get_list_from_edge(row_or_column=2, edge=Edge.TOP)
        self.assertEqual([3, 2, 1], row)
        row = sk.get_list_from_edge(row_or_column=1, edge=Edge.BOTTOM)
        self.assertEqual([3, 1, 2], row)

    def test_validate_works_if_each_number_appears_exactly_once(self):
        sk = create_three_by_three_board()
        is_valid = sk.validate_line(row_or_column=0, edge=Edge.LEFT)
        self.assertTrue(is_valid)

    def test_validate_fails_if_some_number_appears_twice(self):
        sk = create_three_by_three_board()
        sk.set_cell_value(row=0, column=0, value=2)
        is_valid = sk.validate_line(row_or_column=0, edge=Edge.LEFT)
        self.assertFalse(is_valid)
        is_valid = sk.validate_line(row_or_column=0, edge=Edge.RIGHT)
        self.assertFalse(is_valid)

    def test_validate_hint_works_if_hint_is_correct(self):
        sk = create_three_by_three_board()
        is_valid = sk.validate_line_against_hint(row_or_column=0, edge=Edge.LEFT, hint=3)
        self.assertTrue(is_valid)
        is_valid = sk.validate_line_against_hint(row_or_column=0, edge=Edge.RIGHT, hint=1)
        self.assertTrue(is_valid)

    def test_validate_hint_fails_if_doesnt_match(self):
        sk = create_three_by_three_board()
        is_valid = sk.validate_line_against_hint(row_or_column=0, edge=Edge.LEFT, hint=2)
        self.assertFalse(is_valid)
        is_valid = sk.validate_line_against_hint(row_or_column=0, edge=Edge.RIGHT, hint=3)
        self.assertFalse(is_valid)

    def test_RandomizeSolutionIsValidExcludingHint(self):
        sk = create_three_by_three_board()
        sk.randomize_solution()
        for n in range(3):
            is_valid = sk.validate_line(row_or_column=n, edge=Edge.LEFT)
            self.assertTrue(is_valid)
        for n in range(3):
            is_valid = sk.validate_line(row_or_column=n, edge=Edge.TOP)
            self.assertTrue(is_valid)

    def test_RandomizeSolutionIsRandom(self):
        sk = Skyscrapers(size=7)
        sk.randomize_solution()
        board1 = sk.board.copy()
        sk.randomize_solution()
        board2 = sk.board.copy()
        self.assertFalse((board1 == board2).all())

    def test_CanSolveFourByFourWithMinimalHints(self):
        sk = Skyscrapers(4)
        sk.set_hint(row_or_column=0, edge=Edge.RIGHT, hint=2)
        sk.set_hint(row_or_column=1, edge=Edge.LEFT, hint=3)
        sk.set_hint(row_or_column=2, edge=Edge.LEFT, hint=4)
        sk.set_hint(row_or_column=3, edge=Edge.LEFT, hint=2)
        sk.set_hint(row_or_column=3, edge=Edge.TOP, hint=2)
        sk.set_hint(row_or_column=0, edge=Edge.BOTTOM, hint=2)
        sk.set_hint(row_or_column=2, edge=Edge.BOTTOM, hint=3)
        sk.set_hint(row_or_column=3, edge=Edge.BOTTOM, hint=2)
        sk.solve()
        solution = [
            [4, 1, 2, 3],
            [2, 3, 4, 1],
            [1, 2, 3, 4],
            [3, 4, 1, 2]]
        self.assertTrue((sk.board == solution).all())
