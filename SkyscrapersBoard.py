import numpy as np
from typing import Tuple, Dict, List
from enum import Enum, auto


class Edge(Enum):
    TOP = auto()
    BOTTOM = auto()
    LEFT = auto()
    RIGHT = auto()


class Skyscrapers:
    board: np.ndarray  # [row][column]
    size: int
    hints: Dict[Edge, np.ndarray]

    def __init__(self, size):
        self.size = size
        self.board = np.full(shape=(size, size), fill_value=-1)
        self.hints = dict()
        for edge in Edge:
            self.hints[edge] = np.full(shape=size, fill_value=0)

    def get_board_size(self) -> Tuple[int, int]:
        return self.board.shape

    def set_cell_value(self, row: int, column: int, value: int) -> None:
        self.board[row][column] = value

    def get_cell_value(self, row: int, column: int) -> int:
        return self.board[row][column]

    def set_hint(self, row_or_column: int, edge: Edge, hint: int) -> None:
        self.hints[edge][row_or_column] = hint

    def get_hint(self, row_or_column: int, edge: Edge) -> int:
        return self.hints[edge][row_or_column]

    def validate_line(self, row_or_column: int, edge: Edge) -> bool:
        line = self.get_list_from_edge(row_or_column=row_or_column, edge=edge)
        return len(line) == len(set(line))

    def validate_line_against_hint(self, row_or_column: int, edge: Edge, hint: int) -> bool:
        line = self.get_list_from_edge(row_or_column=row_or_column, edge=edge)
        expected_hint = self.compute_hint(line)
        return expected_hint == hint

    def get_list_from_edge(self, row_or_column: int, edge: Edge) -> List[int]:
        answer = np.array([])
        if edge == Edge.LEFT:
            answer = self.board[row_or_column]
        if edge == Edge.RIGHT:
            answer = self.board[row_or_column][::-1]
        if edge == Edge.TOP:
            answer = self.board[:, row_or_column]
        if edge == Edge.BOTTOM:
            answer = self.board[:, row_or_column][::-1]
        return answer.tolist()

    @staticmethod
    def compute_hint(tower_row: List[int]) -> int:
        highest_tower_seen = 0
        total_towers_seen = 0
        for tower in tower_row:
            if tower > highest_tower_seen:
                total_towers_seen += 1
                highest_tower_seen = tower
        return total_towers_seen

    def solve(self):
        solution_found = False
        while not solution_found:
            self.randomize_solution()
            board_is_valid = True
            for edge in Edge:
                for x in range(self.size):
                    hint = self.hints[edge][x]
                    if hint:
                        valid = self.validate_line_against_hint(row_or_column=x, edge=edge, hint=hint)
                        if not valid:
                            board_is_valid = False
                            break
            if board_is_valid:
                solution_found = True

    def randomize_solution(self):
        # start with valid board of [[1,2,...],[2,3,...],[3,4,...],...]
        for i in range(self.size):
            for j in range(self.size):
                self.board[i][j] = (i + 1 + j) % self.size + 1
        # shuffle the rows first
        np.random.shuffle(self.board)
        # now shuffle the columns
        np.random.shuffle(np.transpose(self.board))
